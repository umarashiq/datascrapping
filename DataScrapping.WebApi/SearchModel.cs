﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataScrapping.WebApi
{
    public class SearchModel
    {
        public string SearchText { get; set; }
        public string SellingPrice { get; set; }
        public string Mileage { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public string MakeYear { get; set; }
    }
}
