using DataScrapping.DatabaseLayer;
using DataScrapping.DatabaseLayer.Repositories;
using DataScrapping.WebApi.Helpers;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Filters;
using Swashbuckle.AspNetCore.SwaggerUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataScrapping.WebApi
{
    public class Startup
    {
        public IWebHostEnvironment Env { get; set; }
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            Env = env;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDistributedMemoryCache();
            //services.AddControllers();
            services.AddControllersWithViews();
            services.AddDbContext<ScrapDataDBContext>(opt => opt.UseSqlServer(Configuration["ConnectionString:ScrappedData"]));
            services.AddAuthentication(options =>
            {
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(options =>
            {
                List<SymmetricSecurityKey> symmetricSecurityKeys = new List<SymmetricSecurityKey>
                {
                    new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JWT:key"]))
                };
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    IssuerSigningKeys = symmetricSecurityKeys,
                    ValidIssuer = Configuration["JWT:Issuer"],
                    ValidAudience = Configuration["JWT:Audience"],
                    RequireExpirationTime = true,
                    ValidateIssuer = true,
                    ValidateAudience = false,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true
                };
            });
            services.AddControllers().AddNewtonsoftJson(options =>
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            );
            services.AddMemoryCache();
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true; // consent required
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            services.AddSession(options =>
            {
                options.Cookie.IsEssential = true;
                options.IdleTimeout = TimeSpan.FromSeconds(260);
            });
            services.AddMvc(options => options.EnableEndpointRouting = false).SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
            //services.AddMvc().AddRazorPagesOptions(options => options.AllowRecompilingViewsOnFileChange = true);
            //Install - Package Microsoft.AspNetCore.Mvc.Razor.RuntimeCompilation - Version 5.0.0
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("ScrapCarsData", new OpenApiInfo
                {
                    Version = "ScrapCarsData",
                    Title = "ScrappedData Application Admin Panel Api Architecture",
                    Description = "ScrappedData Application Admin API",
                    TermsOfService = new Uri("http://ScrappedData.pk/terms"),
                    Contact = new OpenApiContact
                    {
                        Name = "ScrappedData Application",
                        Email = string.Empty,
                        Url = new Uri("http://ScrappedData.pk/"),
                    },
                    License = new OpenApiLicense
                    {
                        Name = "Use under LICX",
                        Url = new Uri("http://ScrappedData.pk/license"),
                    }
                });
                c.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"bearer {token}\"",
                    In = ParameterLocation.Header,
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });
                c.AddSecurityRequirement(new OpenApiSecurityRequirement()
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference
                            {
                                Type = ReferenceType.SecurityScheme,
                                Id = "Bearer"
                            },
                            Scheme = "oauth2",
                            Name = "Bearer",
                            In = ParameterLocation.Header,
                        },
                        new List<string>()
                    }
                });
                c.OperationFilter<SecurityRequirementsOperationFilter>();
                c.EnableAnnotations();
                c.ResolveConflictingActions(apiDescriptions => apiDescriptions.First()); //This line
                c.SchemaFilter<SwaggerIgnoreFilter>();
            });
            services.AddSwaggerGenNewtonsoftSupport();
            ////////////////////////////
            ///// Update Scopes  //////
            ///////////////////////////
            services.AddScoped<ICompanyRepository, CompanyRepository>();
            services.AddScoped<ICompanyModelRepository, CompanyModelRepository>();
            services.AddScoped<ISubModelRepository, SubModelRepository>();
            services.AddScoped<ICarAdsRepository, CarAdsRepository>();
            services.AddScoped<IWebsiteRepository, WebsiteRepository>();
            IMvcBuilder builder = services.AddRazorPages();

            #if DEBUG
            if (Env.IsDevelopment())
            {
                builder.AddRazorRuntimeCompilation();
            }
            #endif
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseHttpsRedirection();
            app.UseSession();
            app.UseSwagger();
            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                string swaggerJsonBasePath = string.IsNullOrWhiteSpace(c.RoutePrefix) ? "." : "..";
                c.SwaggerEndpoint($"{swaggerJsonBasePath}/swagger/ScrapCarsData/swagger.json", "ScrapCarsData");
                c.DisplayRequestDuration();
                c.DefaultModelRendering(ModelRendering.Model);
                c.DefaultModelsExpandDepth(1);
                c.DisplayOperationId();
                c.DisplayRequestDuration();
                c.EnableFilter();
                c.ShowExtensions();
                c.EnableValidator();
                c.DocExpansion(DocExpansion.List);
            });
            app.UseRouting();
            //app.UseCors("CorsPolicy");
            app.UseAuthorization();
            //app.UseEndpoints(endpoints =>
            //{
            //    endpoints.MapControllers();
            //});
            app.UseStaticFiles();
            var options = new StaticFileOptions
            {
                ContentTypeProvider = new FileExtensionContentTypeProvider()
            };
            app.UseStaticFiles(options);
            app.UseDefaultFiles();
            var cookiePolicyOptions = new CookiePolicyOptions
            {
                Secure = CookieSecurePolicy.SameAsRequest,
                MinimumSameSitePolicy = SameSiteMode.None
            };
            app.UseCookiePolicy(cookiePolicyOptions);
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=AdsData}/{id?}");
            });
            app.UseMvc();
        }
    }
}
