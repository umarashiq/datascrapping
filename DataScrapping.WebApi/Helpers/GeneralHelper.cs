﻿using DataScraping.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataScrapping.WebApi.Helpers
{
    public class GeneralHelper
    {
        public static void UpdateRequestedFilters(in SearchFilter filters, string paggingConfig)
        {
            filters.Offset = filters.Offset;
            filters.PageSize = filters.PageSize == 0 ? Convert.ToInt32(paggingConfig) : filters.PageSize;

            if (filters.sortFilter != null)
            {
                filters.sortFilter.SortBy = String.IsNullOrEmpty(filters.sortFilter?.SortBy) ? "CreatedOn" : filters.sortFilter?.SortBy;
                filters.sortFilter.Direction = String.IsNullOrEmpty(filters.sortFilter?.Direction) ? "DESC" : filters.sortFilter?.Direction;
            }
        }
    }
}
