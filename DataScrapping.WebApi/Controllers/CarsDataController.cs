﻿using DataScraping.Helpers;
using DataScrapping.DatabaseLayer.Model;
using DataScrapping.DatabaseLayer.Repositories;
using DataScrapping.WebApi.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataScrapping.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CarsDataController : ControllerBase
    {
        /// <summary>
        /// Pressing Alt +Enter 
        /// </summary>
        private readonly ICarAdsRepository _carAdsRepository;
        public CarsDataController(ICarAdsRepository carAdsRepository)
        {
            _carAdsRepository = carAdsRepository;
        }

        [HttpGet("GetAll")]
        public async Task<List<CarAds>> GetAll()
        {
            List<CarAds> response = new List<CarAds>();
            try
            {
                var data = await _carAdsRepository.GetAll();

                return data;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        [HttpGet("UpadteAllPrices")]
        public async Task<List<CarAds>> UpadteAllPrices()
        {
            List<CarAds> response = new List<CarAds>();
            try
            {
                var data = await _carAdsRepository.GetAll();

                return data;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        [HttpGet("GetById")]
        public async Task<CarAds> GetById(int id)
        {
            CarAds response = new CarAds();
            try
            {
                response = null;//await _carAdsRepository.GetById(0);
                return response;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpPost("GetByFilters")]
        public async Task<List<CarAds>> GetByFilters(SearchFilter filterData)
        {
            SearchFilter filters = (SearchFilter)filterData;
            string loginUserId = User.Claims.Where(c => c.Type == "Id").Select(x => x.Value).FirstOrDefault();
            GeneralHelper.UpdateRequestedFilters(filters, "9999");
            try
            {
                var responseData = await _carAdsRepository.GetByFilters(filters);
                return responseData;
            }
            catch(Exception ex)
            {
                return null;
            }
        }
    }
}