﻿using DataScraping.Helpers;
using DataScrapping.DatabaseLayer.Model;
using DataScrapping.DatabaseLayer.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataScrapping.WebApi.Controllers
{
    public class HomeController : Controller
    {
        private readonly ICarAdsRepository _carAdsRepository;
        private readonly IConfiguration _configuration;
        private readonly ICompanyRepository _companyRepository;
        private readonly ICompanyModelRepository _companyModelRepository;
        public HomeController(IConfiguration configuration, ICarAdsRepository carAdsRepository, ICompanyRepository companyRepository, ICompanyModelRepository companyModelRepository)
        {
            _carAdsRepository = carAdsRepository;
            _companyRepository = companyRepository;
            _companyModelRepository = companyModelRepository;
            _configuration = configuration;
        }
        // GET: HomeController
        public ActionResult Login()
        {
            return View();
        }

        // GET: HomeController/Details/5
        public async Task<ActionResult> AdsData()
        {
            List<CarAds> response = new List<CarAds>();
            try
            {
                ViewBag.CarMakersList = await _companyRepository.GetAll();
                ViewBag.MakeModelsList = await _companyModelRepository.GetAll();
                response = await _carAdsRepository.GetAll();
            }
            catch (Exception ex)
            {

            }
            return View(response);
        }
        [HttpPost]
        public async Task<ActionResult> AdsData(IFormCollection formCollection)
        {
            List<CarAds> responseData = new List<CarAds>();
            string make = formCollection["Make"];
            string makeModel = formCollection["MakeModel"];
            string specs = formCollection["Specs"];
            string cylenders = formCollection["NoOfCylinder"];
            string trim = formCollection["Trim"];
            string horsepower = formCollection["Horsepower"];
            string fuelType = formCollection["FuelType"];

            string searchText = formCollection["SearchText"];

            try
            {
                SearchFilter filters = new SearchFilter();
                filters.SearchText = searchText;
                filters.Offset = int.Parse(_configuration["config:offset"].ToString());
                filters.PageSize = int.Parse(_configuration["config:pageSize"].ToString());

                filters.sortFilter = new SortFilter();
                filters.sortFilter.Direction = _configuration["config:sortOrder"].ToString();
                filters.sortFilter.SortBy = _configuration["config:sortBy"].ToString();
                filters.Filters = new List<DatabaseFilter>();
                if (!string.IsNullOrEmpty(specs)) { filters.Filters.Add(new DatabaseFilter() { Name = "Specs", Value = specs, Logic = "and", Operator = "Contains" }); }
                if (!string.IsNullOrEmpty(make)) { filters.Filters.Add(new DatabaseFilter() { Name = "Make", Value = make, Logic = "and", Operator = "Contains" }); }
                if (!string.IsNullOrEmpty(makeModel)) { filters.Filters.Add(new DatabaseFilter() { Name = "Model", Value = makeModel, Logic = "and", Operator = "Contains" }); }
                if (!string.IsNullOrEmpty(cylenders)) { filters.Filters.Add(new DatabaseFilter() { Name = "Cylenders", Value = cylenders, Logic = "and", Operator = "Contains" }); }
                if (!string.IsNullOrEmpty(trim)) { filters.Filters.Add(new DatabaseFilter() { Name = "Trim", Value = trim, Logic = "and", Operator = "Contains" }); }
                if (!string.IsNullOrEmpty(horsepower)) { filters.Filters.Add(new DatabaseFilter() { Name = "Horsepower", Value = horsepower, Logic = "and", Operator = "Contains" }); }
                if (!string.IsNullOrEmpty(fuelType)) { filters.Filters.Add(new DatabaseFilter() { Name = "FuleType", Value = horsepower, Logic = "and", Operator = "Contains" }); }

                responseData = await _carAdsRepository.GetByFilters(filters);
                ViewBag.CarMakersList = await _companyRepository.GetAll();
                ViewBag.MakeModelsList = await _companyModelRepository.GetAll();
            }
            catch (Exception ex)
            {

            }
            return View(responseData);
        }
        // GET: HomeController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: HomeController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: HomeController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: HomeController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: HomeController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: HomeController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
