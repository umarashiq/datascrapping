﻿using DataScrapping.DatabaseLayer.Model;
using DataScrapping.DatabaseLayer.Repositories;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace DataScrappingApplication
{
    public partial class DubiCar_Form : Form
    {
        private readonly ICompanyRepository _companyRepository;
        private readonly ICompanyModelRepository _companyModelRepository;
        private readonly ISubModelRepository _subModelRepository;
        private readonly ICarAdsRepository _carAdsRepository;
        public string Base_Url_Page_Text = "?page=";
        public DubiCar_Form(ICompanyRepository companyRepository, ICompanyModelRepository companyModelRepository, ISubModelRepository subModelRepository, ICarAdsRepository carAdsRepository)
        {
            InitializeComponent();
            _companyRepository = companyRepository;
            _companyModelRepository = companyModelRepository;
            _subModelRepository = subModelRepository;
            _carAdsRepository = carAdsRepository;
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            try
            {
                //var url = PageUrl.Text;
                //var url = "https://www.dubicars.com/dubai/used";
                HtmlWeb web = new HtmlWeb();
                //var htmlDoc = web.Load(url);

                //#region Scrapping all model, model_types and sub_models

                //#region Scrapping Companies Record...
                //var node = htmlDoc?.DocumentNode?.SelectNodes("//div[@id='search-link-box']//li");
                //List<Company> companies = new List<Company>();
                //foreach (var item in node)
                //{
                //    Company company = new Company();
                //    var CompanyUrlResult = item.ChildNodes;
                //    company.CompanyName = FilterString(CompanyUrlResult[1].InnerText);
                //    company.Url = CompanyUrlResult[1].Attributes["href"].Value;
                //    //Update Ads Count
                //    company.AdsCount = CompanyUrlResult[2].InnerText;
                //    var TotalAds = company.AdsCount.Split("(");
                //    TotalAds = TotalAds[1].Split(")");
                //    company.AdsCount = TotalAds[0];
                //    company.Url = "https://www.dubicars.com" + company.Url;
                //    company.Id = 0;
                //    var Id = await _companyRepository.Save(company);
                //    companies.Add(new Company()
                //    {
                //        Id = (int)Id,
                //        AdsCount = TotalAds[0],
                //        CompanyName = FilterString(CompanyUrlResult[1].InnerText),
                //        Url = company.Url
                //    });
                //}
                //#endregion

                //#region Scrapping CompaniesModels...
                //List<CompanyModel> companyModel = new List<CompanyModel>();
                //string companyModelUrl = "";
                //foreach (var companiesList in companies)
                //{
                //    companyModelUrl = companiesList.Url;
                //    CompanyModel model = new CompanyModel();
                //    model.CompanyId = companiesList.Id;

                //    htmlDoc = web.Load(companyModelUrl);

                //    var nodes = htmlDoc?.DocumentNode?.SelectNodes("//div[@id='search-link-box']//li");
                //    foreach (var item in nodes)
                //    {
                //        var CompanyModelUrlResult = item.ChildNodes;
                //        model.ModelName = FilterString(CompanyModelUrlResult[1].InnerText);
                //        //model.CompanyId = companiesList.Id;
                //        model.Url = CompanyModelUrlResult[1].Attributes["href"].Value;
                //        //Update Ads Count
                //        model.AdsCount = CompanyModelUrlResult[2].InnerText;
                //        var TotalAds = model.AdsCount.Split("(");
                //        TotalAds = TotalAds[1].Split(")");
                //        model.AdsCount = TotalAds[0];
                //        model.Url = "https://www.dubicars.com" + model.Url;
                //        model.Id = 0;
                //        long Id = 0;
                //        try
                //        {
                //            Id = await _companyModelRepository.Save(model);
                //        }
                //        catch (Exception ex)
                //        {

                //            throw;
                //        }
                //        companyModel.Add(new CompanyModel()
                //        {
                //            Id = (int)Id,
                //            AdsCount = TotalAds[0],
                //            CompanyId = companiesList.Id,
                //            ModelName = FilterString(CompanyModelUrlResult[1].InnerText),
                //            Url = model.Url
                //        });
                //    }
                //}
                //#endregion

                #region scrapping submodels...
                //list<submodel> submodellist = new list<submodel>();
                //string submodelurl = "";
                //foreach (var modelslist in companymodel)
                //{
                //    submodelurl = modelslist.url;
                //    submodel submodel = new submodel();
                //    submodel.modelid = modelslist.id;

                //    htmldoc = web.load(companymodelurl);

                //    var submodelnodes = htmldoc?.documentnode?.selectnodes("//div[@id='search-link-box']//li");
                //    foreach (var item in submodelnodes)
                //    {
                //        var companymodelurlresult = item.childnodes;
                //        submodel.submodelname = filterstring(companymodelurlresult[1].innertext);
                //        //model.companyid = companieslist.id;
                //        submodel.url = companymodelurlresult[1].attributes["href"].value;
                //        //update ads count
                //        submodel.adscount = companymodelurlresult[2].innertext;
                //        var totalads = submodel.adscount.split("(");
                //        totalads = totalads[1].split(")");
                //        submodel.adscount = totalads[0];
                //        submodel.url = "https://www.dubicars.com" + submodel.url;
                //        submodel.id = 0;
                //        long id = 0;
                //        try
                //        {
                //            id = await _submodelrepository.save(submodel);
                //        }
                //        catch (exception ex)
                //        {

                //            throw;
                //        }
                //        submodellist.add(new submodel()
                //        {
                //            id = (int)id,
                //            url = model.url,
                //            modelid = modelslist.id,
                //            submodelname = filterstring(companymodelurlresult[1].innertext),
                //            adscount = totalads[0]
                //        });
                //    }
                //}
                #endregion
                //#endregion

                #region Scrapping Models Ads...
                List<Company> companiesData = await _companyRepository.GetAll();
                List<CarAds> dubizzleCarsList = new List<CarAds>();
                for (int i = 0; i < companiesData.Count; i++)
                {
                    int totalPages = Convert.ToInt32(companiesData[i].AdsCount) / 30;
                    for (int j = 0; j < totalPages; j++)
                    {
                        string page_No_Str = Base_Url_Page_Text + j;
                        var Url = companiesData[i].Url;

                        Url += page_No_Str;

                        //companiesData[i].Url += page_No_Str;

                        var htmlDocs = (new HtmlWeb()).Load(Url);
                        var nodes = htmlDocs?.DocumentNode?.SelectNodes("//section[@id='serp-list-new']//li");

                        foreach (var itemNode in nodes)
                        {
                            CarAds carAds = new CarAds();
                            string make = "", model = "",  color = "", bodyType = "", exportStatus = "", specs = "", transmission = "", fuleType = "", seats = "", cylenders = "";
                            try
                            {
                                var innerNodeHtml = new HtmlAgilityPack.HtmlDocument();
                                innerNodeHtml.LoadHtml(itemNode.OuterHtml);
                                carAds.WebsiteId = 2;
                                var TitleNode = innerNodeHtml?.DocumentNode?.SelectSingleNode("//div[@class='bp-2-show description']//h3//a") ?? null;
                                carAds.LinkofCar = TitleNode?.Attributes["href"]?.Value ?? null;
                                carAds.Title = FilterString(innerNodeHtml?.DocumentNode?.SelectSingleNode("//div[@class='bp-2-show description']//h3//a")?.InnerText);
                                carAds.MakeYear = FilterString(innerNodeHtml?.DocumentNode?.SelectSingleNode("//div[@class='bp-2-show description']//div[@class='bp-2-show year']")?.InnerText);
                                carAds.Millage = FilterString(innerNodeHtml?.DocumentNode?.SelectSingleNode("//div[@class='bp-2-show description']//div[@class='bp-2-show mileage']")?.InnerText);
                                carAds.Location = "N/A";
                                carAds.SellingPrice = FilterString(innerNodeHtml?.DocumentNode?.SelectSingleNode("//div[@class='bp-2-show price-block']//strong[@class='money total-price']")?.InnerText);
                                if (carAds.LinkofCar != null)
                                {
                                    var detailsHtmlPage = (new HtmlWeb())?.Load(carAds.LinkofCar) ?? null;
                                    var innerLink = detailsHtmlPage?.DocumentNode?.SelectNodes("//section[@id='item-details']//td");

                                    make = FilterString(innerLink[0]?.InnerText) ?? null;
                                    if (make != null)
                                    {
                                        carAds.Make = make;
                                    }
                                    model = FilterString(innerLink[1]?.InnerText) ?? null;
                                    if (model != null)
                                    {
                                        carAds.Model = model;
                                    }
                                    //model.Wheels = FilterString(innerLink[3]?.InnerText) ?? null;
                                    color = FilterString(innerLink[4]?.InnerText) ?? null;
                                    if (color != null)
                                    {
                                        carAds.Color = color;
                                    }
                                    bodyType = FilterString(innerLink[5]?.InnerText) ?? null;
                                    if (bodyType != null)
                                    {
                                        carAds.BodyType = bodyType;
                                    }
                                    exportStatus = FilterString(innerLink[7]?.InnerText) ?? null;
                                    if (exportStatus != null)
                                    {
                                        carAds.ExportStatus = exportStatus;
                                    }
                                    specs = FilterString(innerLink[8]?.InnerText) ?? null;
                                    if (specs != null)
                                    {
                                        carAds.Specs = specs;
                                    }
                                    transmission = FilterString(innerLink[9]?.InnerText) ?? null;
                                    if (transmission != null)
                                    {
                                        carAds.Transmission = transmission;
                                    }
                                    fuleType = FilterString(innerLink[10]?.InnerText) ?? null;
                                    if (fuleType != null)
                                    {
                                        carAds.FuleType = fuleType;
                                    }
                                    seats = FilterString(innerLink[11]?.InnerText) ?? null;
                                    if (seats != null)
                                    {
                                        carAds.Seats = seats;
                                    }
                                    cylenders = FilterString(innerLink[12]?.InnerText) ?? null;
                                    if (cylenders != null)
                                    {
                                        carAds.Cylenders = cylenders;
                                    }
                                    //model.Interior = FilterString(innerLink[13]?.InnerText) ?? null;
                                    //model.SteeringSide = FilterString(innerLink[14]?.InnerText) ?? null;
                                }

                                await _carAdsRepository.Save(carAds);
                            }
                            catch (Exception ex)
                            {
                                throw;
                            }
                        }
                    }
                }
                #endregion


            }
            catch (Exception ex)
            {
                throw;
            }
        }
        //#endregion

        public string FilterString(string text)
        {
            if (!string.IsNullOrEmpty(text))
            {
                var responseData = text.Replace("\n", "").Replace("\t", " ").Replace("&#8234;", " ").Replace("&gt", " ").Trim();
                return responseData;
            }
            return "";
        }

        private void DubiCar_Form_Load(object sender, EventArgs e)
        {

        }
    }
}