﻿using System;
using HtmlAgilityPack;
using System.Threading;
using System.Windows.Forms;
using DataScrapping.DatabaseLayer.Model;
using DataScrapping.DatabaseLayer.Repositories;
using System.Collections.Generic;

namespace DataScrappingApplication
{
    public partial class Dubizzle_Form : Form
    {
        private readonly ICompanyRepository _companyRepository;
        private readonly ICompanyModelRepository _companyModelRepository;
        private readonly ISubModelRepository _subModelRepository;
        private readonly IDubizzleCarsRepository _dubizzleCarsRepository;
        public Dubizzle_Form(ICompanyRepository companyRepository, ICompanyModelRepository companyModelRepository, ISubModelRepository subModelRepository, IDubizzleCarsRepository dubizzleCarsRepository)
        {
            _companyRepository = companyRepository;
            _companyModelRepository = companyModelRepository;
            _subModelRepository = subModelRepository;
            _dubizzleCarsRepository = dubizzleCarsRepository;
            InitializeComponent();
        }
        private async void button1_Click(object sender, EventArgs e)
        {
            try
            {
                List<int> pageNo = new List<int>() { 1 };
                //int atmp = 0;
                //var modelCompanies = await _companyModelRepository.GetAll();
                //for (int i = 0; i < modelCompanies.Count; i++)
                //{
                //Decimal totalPages = Convert.ToDecimal(modelCompanies[i].AdsCount) / 25;
                //    totalPages = Math.Ceiling(totalPages);
                //    totalPages = ((totalPages > 40) ? 40 : totalPages);
                for (int j = 1; j <= 40; j++)
                {
                    try
                    {
                        var url = pageUrl.Text; /// https://dubai.dubizzle.com/motors/used-cars/
                        //string baseUrl = "?page=" + j;
                        //string Title = string.Empty, Model = string.Empty, MakeYear = string.Empty, Location = string.Empty, ModelType = string.Empty, Price = string.Empty, Mileage = string.Empty;
                        //DateTime PostingDate = new DateTime();
                        url = url + j;
                        HtmlWeb web = new HtmlWeb();
                        Thread.Sleep(2000);
                        var htmlDoc = await web.LoadFromWebAsync(url);
                        var node = htmlDoc?.DocumentNode?.SelectNodes("//div[@class='list-item-wrapper']");
                        Thread.Sleep(2000);
                        if (node == null)
                        {
                            //if (atmp <= 5)
                            //{
                            //    i--;
                            //    atmp++;
                            continue;
                            //}
                        }
                        if (node != null)
                            foreach (var item in node)
                            {
                                var innerNodeHtml = new HtmlAgilityPack.HtmlDocument();
                                innerNodeHtml.LoadHtml(item.OuterHtml);
                                CarAds dubizzleCars = new CarAds();
                                var title = innerNodeHtml?.DocumentNode?.SelectSingleNode("//div[@class='block item-title']//h2[@id='title']//span[@class='title']//a");
                                dubizzleCars.LinkofCar = title.Attributes["href"]?.Value;
                                dubizzleCars.Title = FilterString(title.InnerText);
                                dubizzleCars.SellingPrice = FilterString(innerNodeHtml?.DocumentNode?.SelectSingleNode("//div[@class='block item-title']//div[@class='price']//span[@class='selling-price__amount']").InnerText).Replace(" ", string.Empty);
                                var description = FilterString(innerNodeHtml?.DocumentNode?.SelectSingleNode("//p[@class='breadcrumbs']")?.InnerHtml).Split(";");
                                dubizzleCars.Make = description[1];
                                dubizzleCars.Model = description[2];
                                dubizzleCars.PostingDate = innerNodeHtml?.DocumentNode?.SelectSingleNode("//p[@class='date']")?.InnerHtml;
                                dubizzleCars.Location = FilterString(innerNodeHtml?.DocumentNode?.SelectSingleNode("//div[@class='location']").InnerText).Replace(" ", string.Empty);

                                var yearKm = innerNodeHtml?.DocumentNode?.SelectNodes("//ul[@class='features']");
                                HtmlWeb detailsWebPage = new HtmlWeb();
                                var detailHtmlDoc = detailsWebPage.Load(url);
                                Thread.Sleep(2000);
                                var carAdDetails = detailHtmlDoc.DocumentNode.SelectNodes("//div[@id='listing-details-list-with-margin-top']//li");
                                if (carAdDetails == null)
                                { continue; }

                                dubizzleCars.MakeYear = FilterString(carAdDetails[1]?.InnerText).Replace("Year", " ").Trim();
                                dubizzleCars.Millage = FilterString(carAdDetails[2]?.InnerText).Replace("Kilometers", "").Trim();
                                dubizzleCars.Color = FilterString(carAdDetails[11]?.InnerText).Replace("Color", "").Trim();
                                dubizzleCars.Doors = FilterString(carAdDetails[12]?.InnerText).Replace("Doors", "").Trim();
                                dubizzleCars.Warranty = FilterString(carAdDetails[3]?.InnerText).Replace("Warranty", "").Trim();
                                dubizzleCars.Specs = FilterString(carAdDetails[4]?.InnerText).Replace("Specs", "").Trim();
                                dubizzleCars.Transmission = FilterString(carAdDetails[5]?.InnerText).Replace("Transmission", "").Trim();
                                dubizzleCars.BodyType = FilterString(carAdDetails[6]?.InnerText).Replace("Body Type", "").Trim();
                                dubizzleCars.Trim = FilterString(carAdDetails[7]?.InnerText).Replace("Trim", "").Trim();
                                dubizzleCars.BodyCondititon = FilterString(carAdDetails[8]?.InnerText).Replace("Body Condition", "").Trim();
                                dubizzleCars.FuleType = FilterString(carAdDetails[10]?.InnerText).Replace("Fuel Type", "").Trim();
                                dubizzleCars.Cylenders = FilterString(carAdDetails[13]?.InnerText).Replace("Cylinders", "").Trim();
                                dubizzleCars.SallerType = FilterString(carAdDetails[14]?.InnerText).Replace("Seller Type", "").Trim();
                                dubizzleCars.Horsepower = FilterString(carAdDetails[15]?.InnerText).Replace("Horsepower", "").Trim();
                                dubizzleCars.Id = 0;
                                dubizzleCars.WebsiteId = 1;
                                await _dubizzleCarsRepository.Save(dubizzleCars);
                            }
                        ResultShow.Text = "Data saved of \"" + url + "\"";
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                }

                //var PostingDate = await _dubizzleCarsRepository.GetAll();
                //foreach (var item in PostingDate)
                //{
                //    var url = item.LinkofCar;// link form dbo Dubizzelcar result
                //    HtmlWeb web = new HtmlWeb();
                //    var htmlDoc = web.Load(url);
                //    var node = htmlDoc?.DocumentNode?.SelectSingleNode("//div[@id='listing-main-col']");
                //    var LiNodes = node?.SelectNodes("//li");
                //}
                ResultShow.Text = "Task has been completed ";
                /////old code
                //List<Company> companies = new List<Company>();
                //var CarCompanies = htmlDoc?.DocumentNode?.SelectNodes("//div[@id='browse-in-category-list']//div[@class='browse_in_list']//div[@id='hide']//ul[@class='browse_in_widget_col col-span-3']//li");
                //foreach (var record in CarCompanies)
                //{
                //    Company company = new Company();
                //    var CopamyayURLResult = record.ChildNodes;
                //    company.CompanyName = FilterString(CopamyayURLResult[1].InnerText);
                //    company.Url = CopamyayURLResult[1].Attributes["href"].Value;
                //    //Update Ads Count
                //    company.AdsCount = CopamyayURLResult[3].InnerText;
                //    var TotalAds = company.AdsCount.Split("(");
                //    TotalAds = TotalAds[1].Split(")");
                //    company.AdsCount = TotalAds[0];
                //    company.Url = "https://dubai.dubizzle.com/" + company.Url;
                //    var Id = await _companyRepository.Save(company);
                //    companies.Add(new Company()
                //    {
                //        Id = (int)Id,
                //        AdsCount = TotalAds[0],
                //        CompanyName = FilterString(CopamyayURLResult[1].InnerText),
                //        Url = company.Url
                //    });
                //}
                //List<CompanyModel> companyModels = new List<CompanyModel>(); 
                //foreach (var item in companies)
                //{
                //    url = item.Url;
                //    htmlDoc = web.Load(url);
                //    var CarModels = htmlDoc?.DocumentNode?.SelectNodes("//div[@id='browse-in-category-list']//div[@id='show']//li");
                //        if (CarModels != null)
                //        foreach (var record in CarModels)
                //        {
                //            CompanyModel companyModel = new CompanyModel();
                //            var CopamyayURLResult = record.ChildNodes;
                //            companyModel.ModelName= FilterString(CopamyayURLResult[1].InnerText);
                //            companyModel.CompanyId = item.Id;
                //            companyModel.Url = CopamyayURLResult[1].Attributes["href"].Value;
                //            //Update Ads Count
                //            companyModel.AdsCount = CopamyayURLResult[3].InnerText;
                //            var TotalAds = companyModel.AdsCount.Split("(");
                //            TotalAds = TotalAds[1].Split(")");
                //            companyModel.AdsCount = TotalAds[0];
                //            companyModel.Url = "https://dubai.dubizzle.com/" + companyModel.Url;
                //            var Id = await _companyModelRepository.Save(companyModel);
                //            companyModels.Add(new CompanyModel() 
                //            {
                //                Id= (int)Id,
                //                CompanyId= item.Id,
                //                ModelName= FilterString(CopamyayURLResult[1].InnerText),
                //                AdsCount= TotalAds[0],
                //                Url= companyModel.Url
                //            });
                //        }
                //}
                //foreach (var item in companyModels)
                //{
                //    url = item.Url;
                //    htmlDoc = web.Load(url);
                //    var CarModels = htmlDoc?.DocumentNode?.SelectNodes("//div[@id='browse-in-category-list']//div[@id='show']//li");
                //    if (CarModels != null)
                //        foreach (var record in CarModels)
                //        {
                //            SubModel companySubModel = new SubModel();
                //            var CopamyayURLResult = record.ChildNodes;
                //            companySubModel.SubModelName = FilterString(CopamyayURLResult[1].InnerText);
                //            companySubModel.ModelId = item.Id;
                //            companySubModel.Url = CopamyayURLResult[1].Attributes["href"].Value;
                //            //Update Ads Count
                //            companySubModel.AdsCount = CopamyayURLResult[3].InnerText;
                //            companySubModel.AdsCount =0.ToString();
                //            companySubModel.Url = "https://dubai.dubizzle.com/" + companySubModel.Url;
                //            var Id = await _subModelRepository.Save(companySubModel);
                //        }
                //}
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public string FilterString(string text)
        {
            var responseData = text.Replace("\n", "").Replace("\t", " ").Replace("&#8234;", " ").Replace("&gt", " ").Replace("<strong>", " ").Replace("</strong>", " ").Trim();
            return responseData;
        }
        private void Form1_Load(object sender, EventArgs e)
        {
        }
        private void button2_Click(object sender, EventArgs e)
        {
            //DubiCars dubiCars = new DataScrappingApplication.DubiCars();
        }
        private void label2_Click(object sender, EventArgs e)
        {
        }
    }
}
