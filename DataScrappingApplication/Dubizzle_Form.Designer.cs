﻿namespace DataScrappingApplication
{
    partial class Dubizzle_Form
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.pageUrl = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ResultShow = new System.Windows.Forms.Label();
            this.label_Result = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button1.Location = new System.Drawing.Point(321, 181);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(176, 41);
            this.button1.TabIndex = 0;
            this.button1.Text = "Scrap Data From Url";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pageUrl
            // 
            this.pageUrl.Location = new System.Drawing.Point(50, 139);
            this.pageUrl.Name = "pageUrl";
            this.pageUrl.Size = new System.Drawing.Size(719, 27);
            this.pageUrl.TabIndex = 1;
            this.pageUrl.Text = "https://dubai.dubizzle.com/motors/used-cars/?page=";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(50, 105);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Enter Page URL";
            // 
            // ResultShow
            // 
            this.ResultShow.AutoSize = true;
            this.ResultShow.Location = new System.Drawing.Point(50, 263);
            this.ResultShow.Name = "ResultShow";
            this.ResultShow.Size = new System.Drawing.Size(56, 20);
            this.ResultShow.TabIndex = 2;
            this.ResultShow.Text = "Result: ";
            this.ResultShow.Click += new System.EventHandler(this.label2_Click);
            // 
            // label_Result
            // 
            this.label_Result.AutoSize = true;
            this.label_Result.Location = new System.Drawing.Point(112, 263);
            this.label_Result.Name = "label_Result";
            this.label_Result.Size = new System.Drawing.Size(0, 20);
            this.label_Result.TabIndex = 2;
            // 
            // Dubizzle_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(837, 525);
            this.Controls.Add(this.label_Result);
            this.Controls.Add(this.ResultShow);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pageUrl);
            this.Controls.Add(this.button1);
            this.Name = "Dubizzle_Form";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox pageUrl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label ResultShow;
        private System.Windows.Forms.Label label_Result;
    }
}

