﻿
namespace DataScrappingApplication
{
    partial class Website_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Title = new System.Windows.Forms.Label();
            this.WebsiteNameLabel = new System.Windows.Forms.Label();
            this.WebsiteNameInput = new System.Windows.Forms.TextBox();
            this.Save = new System.Windows.Forms.Button();
            this.WebsiteDataGridView = new System.Windows.Forms.DataGridView();
            this.Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WebsiteName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BaseUrl = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WebsiteUrlInput = new System.Windows.Forms.TextBox();
            this.Website = new System.Windows.Forms.Label();
            this.editBtn = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.ResultShow = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.WebsiteDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // Title
            // 
            this.Title.AutoSize = true;
            this.Title.Font = new System.Drawing.Font("Segoe UI Semibold", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.Title.Location = new System.Drawing.Point(137, 32);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(197, 31);
            this.Title.TabIndex = 0;
            this.Title.Text = "Manage Websites";
            // 
            // WebsiteNameLabel
            // 
            this.WebsiteNameLabel.AutoSize = true;
            this.WebsiteNameLabel.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.WebsiteNameLabel.Location = new System.Drawing.Point(15, 101);
            this.WebsiteNameLabel.Name = "WebsiteNameLabel";
            this.WebsiteNameLabel.Size = new System.Drawing.Size(139, 28);
            this.WebsiteNameLabel.TabIndex = 1;
            this.WebsiteNameLabel.Text = "Website Name";
            // 
            // WebsiteNameInput
            // 
            this.WebsiteNameInput.Location = new System.Drawing.Point(15, 132);
            this.WebsiteNameInput.Name = "WebsiteNameInput";
            this.WebsiteNameInput.Size = new System.Drawing.Size(460, 27);
            this.WebsiteNameInput.TabIndex = 0;
            // 
            // Save
            // 
            this.Save.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Save.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.Save.Location = new System.Drawing.Point(15, 255);
            this.Save.Name = "Save";
            this.Save.Size = new System.Drawing.Size(139, 39);
            this.Save.TabIndex = 2;
            this.Save.Text = "Save";
            this.Save.UseVisualStyleBackColor = false;
            this.Save.Click += new System.EventHandler(this.save_button_Click);
            // 
            // WebsiteDataGridView
            // 
            this.WebsiteDataGridView.ColumnHeadersHeight = 29;
            this.WebsiteDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.WebsiteDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Id,
            this.WebsiteName,
            this.BaseUrl});
            this.WebsiteDataGridView.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.WebsiteDataGridView.Location = new System.Drawing.Point(0, 341);
            this.WebsiteDataGridView.MultiSelect = false;
            this.WebsiteDataGridView.Name = "WebsiteDataGridView";
            this.WebsiteDataGridView.RowHeadersVisible = false;
            this.WebsiteDataGridView.RowHeadersWidth = 51;
            this.WebsiteDataGridView.RowTemplate.Height = 29;
            this.WebsiteDataGridView.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.WebsiteDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.WebsiteDataGridView.ShowCellErrors = false;
            this.WebsiteDataGridView.ShowCellToolTips = false;
            this.WebsiteDataGridView.ShowEditingIcon = false;
            this.WebsiteDataGridView.ShowRowErrors = false;
            this.WebsiteDataGridView.Size = new System.Drawing.Size(507, 259);
            this.WebsiteDataGridView.TabIndex = 4;
            this.WebsiteDataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.WebsiteDataGridView_CellContentClick);
            // 
            // Id
            // 
            this.Id.HeaderText = "Id";
            this.Id.MinimumWidth = 6;
            this.Id.Name = "Id";
            this.Id.ReadOnly = true;
            this.Id.Width = 45;
            // 
            // WebsiteName
            // 
            this.WebsiteName.HeaderText = "Website Name";
            this.WebsiteName.MinimumWidth = 6;
            this.WebsiteName.Name = "WebsiteName";
            this.WebsiteName.Width = 160;
            // 
            // BaseUrl
            // 
            this.BaseUrl.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.BaseUrl.HeaderText = "Base Url";
            this.BaseUrl.MinimumWidth = 6;
            this.BaseUrl.Name = "BaseUrl";
            // 
            // WebsiteUrlInput
            // 
            this.WebsiteUrlInput.Location = new System.Drawing.Point(15, 204);
            this.WebsiteUrlInput.Name = "WebsiteUrlInput";
            this.WebsiteUrlInput.Size = new System.Drawing.Size(460, 27);
            this.WebsiteUrlInput.TabIndex = 1;
            // 
            // Website
            // 
            this.Website.AutoSize = true;
            this.Website.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Website.Location = new System.Drawing.Point(15, 173);
            this.Website.Name = "Website";
            this.Website.Size = new System.Drawing.Size(86, 28);
            this.Website.TabIndex = 5;
            this.Website.Text = "Base Url:";
            // 
            // editBtn
            // 
            this.editBtn.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.editBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.editBtn.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.editBtn.ForeColor = System.Drawing.Color.Orange;
            this.editBtn.Location = new System.Drawing.Point(177, 255);
            this.editBtn.Name = "editBtn";
            this.editBtn.Size = new System.Drawing.Size(134, 39);
            this.editBtn.TabIndex = 2;
            this.editBtn.Text = "Update";
            this.editBtn.UseVisualStyleBackColor = false;
            this.editBtn.Click += new System.EventHandler(this.editBtn_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.PeachPuff;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.button2.ForeColor = System.Drawing.Color.Crimson;
            this.button2.Location = new System.Drawing.Point(333, 255);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(142, 39);
            this.button2.TabIndex = 2;
            this.button2.Text = "Delete";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.delete_button_Click);
            // 
            // ResultShow
            // 
            this.ResultShow.AutoSize = true;
            this.ResultShow.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.ResultShow.Location = new System.Drawing.Point(12, 310);
            this.ResultShow.Name = "ResultShow";
            this.ResultShow.Size = new System.Drawing.Size(108, 28);
            this.ResultShow.TabIndex = 7;
            this.ResultShow.Text = "ResultNote";
            // 
            // Website_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(507, 600);
            this.Controls.Add(this.ResultShow);
            this.Controls.Add(this.WebsiteUrlInput);
            this.Controls.Add(this.Website);
            this.Controls.Add(this.WebsiteDataGridView);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.editBtn);
            this.Controls.Add(this.Save);
            this.Controls.Add(this.WebsiteNameInput);
            this.Controls.Add(this.WebsiteNameLabel);
            this.Controls.Add(this.Title);
            this.Name = "Website_Form";
            this.Text = "Website_Form";
            ((System.ComponentModel.ISupportInitialize)(this.WebsiteDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Title;
        private System.Windows.Forms.Label WebsiteNameLabel;
        private System.Windows.Forms.TextBox WebsiteNameInput;
        private System.Windows.Forms.Button Save;
        private System.Windows.Forms.DataGridView WebsiteDataGridView;
        private System.Windows.Forms.TextBox WebsiteUrlInput;
        private System.Windows.Forms.Label Website;
        private System.Windows.Forms.Button editBtn;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label ResultShow;
        private System.Windows.Forms.DataGridViewTextBoxColumn Id;
        private System.Windows.Forms.DataGridViewTextBoxColumn WebsiteName;
        private System.Windows.Forms.DataGridViewTextBoxColumn BaseUrl;
    }
}