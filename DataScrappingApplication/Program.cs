using DataScrapping.DatabaseLayer;
using DataScrapping.DatabaseLayer.Repositories;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Windows.Forms;
namespace DataScrappingApplication
{
    static class Program
    {

        [STAThread]
        static void Main()
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var services = new ServiceCollection();

            ConfigureServices(services);

            using (ServiceProvider serviceProvider = services.BuildServiceProvider())
            {
                var dubizzle = serviceProvider.GetRequiredService<Dubizzle_Form>();
                Application.Run(dubizzle);

                //var dubiCars = serviceProvider.GetRequiredService<DubiCar_Form>();
                //Application.Run(dubiCars);

                //var website = serviceProvider.GetRequiredService<Website_Form>();
                //Application.Run(website);

                //var carAdDetails = serviceProvider.GetRequiredService<CarAdDetails>();
                //Application.Run(carAdDetails);
            }
        }

        private static void ConfigureServices(ServiceCollection services)
        {
            //services.AddScoped<ICarsModelRepository, CarsModelRepository>();
            services
             .AddDbContext<ScrapDataDBContext>(options => options.UseSqlServer("server=.;database=DataScrappingDB;trusted_connection=true;"))
            .AddSingleton<Dubizzle_Form>()
            .AddSingleton<DubiCar_Form>()
            .AddSingleton<Website_Form>()
            .AddSingleton<CarAdDetails>()
            .AddScoped<ICompanyRepository, CompanyRepository>();
            #region Interface Scopes...
            services.AddScoped<ICompanyModelRepository, CompanyModelRepository>();
            services.AddScoped<ISubModelRepository,SubModelRepository>();
            services.AddScoped<ICarAdsRepository,CarAdsRepository>();
            services.AddScoped<IDubizzleCarsRepository, DubizzleCarsRepository>();
            services.AddSingleton<DubiCar_Form>();
            services.AddScoped<IWebsiteRepository,WebsiteRepository>();
            #endregion
            
            //services.AddScoped<ISubModelRepository, SubModelRepository>();
            //services.AddScoped<IWebsiteRepository, WebsiteRepository>();
        }
    }
}
