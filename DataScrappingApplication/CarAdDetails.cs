﻿using DataScrapping.DatabaseLayer.Model;
using DataScrapping.DatabaseLayer.Repositories;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DataScrappingApplication
{
    public partial class CarAdDetails : Form
    {
        private readonly ICarAdsRepository _carAdsRepository;
        private readonly ICompanyRepository _companyRepository;
        private readonly IDubizzleCarsRepository _dubizzleCarsRepository;
        public CarAdDetails(ICarAdsRepository carAdsRepository, ICompanyRepository companyRepository, IDubizzleCarsRepository dubizzleCarsRepository)
        {
            InitializeComponent();
            _carAdsRepository = carAdsRepository;
            _companyRepository = companyRepository;
            _dubizzleCarsRepository = dubizzleCarsRepository;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private async void CarAdDetails_Load(object sender, EventArgs e)
        {
            var data = await _carAdsRepository.GetAll();
            foreach (var item in data)
            {
                CarAdsDataGridView.Rows.Add(item.Id, item.Title, item.Model, item.MakeYear, item.SellingPrice, item.Millage, item.Color, item.Specs, item.Transmission, item.BodyType, item.FuleType, item.Cylenders, item.Seats);
            }
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            #region DubizzleAdsDetails....

            var url = "https://dubai.dubizzle.com/motors/used-cars/audi/a3/2020/8/25/audi-a3-30-tfsi-2017-gcc-under-warranty-wi-2/?back=L21vdG9ycy91c2VkLWNhcnMvYXVkaS8%3D&pos=1";
            HtmlWeb web = new HtmlWeb();
            var htmlDoc = await web.LoadFromWebAsync(url);
            var carAdDetails = htmlDoc.DocumentNode.SelectNodes("//div[@id='listing-details-list-with-margin-top']//li");
            foreach (var item in carAdDetails)
            {
                try
                {

                    CarAds dubizzleCars = new CarAds();

                    //var titleChildNodes = item.ChildNodes;

                    dubizzleCars.MakeYear = FilterString(carAdDetails[1]?.InnerText).Replace("Year", " ").Trim();
                    dubizzleCars.Millage = FilterString(carAdDetails[2]?.InnerText).Replace("Kilometers", "").Trim();
                    dubizzleCars.Warranty = FilterString(carAdDetails[3]?.InnerText).Replace("Warranty", "").Trim();
                    dubizzleCars.Specs = FilterString(carAdDetails[4]?.InnerText).Replace("Specs", "").Trim();
                    dubizzleCars.Transmission = FilterString(carAdDetails[5]?.InnerText).Replace("Transmission", "").Trim();
                    dubizzleCars.BodyType = FilterString(carAdDetails[6]?.InnerText).Replace("Body Type", "").Trim();
                    dubizzleCars.Trim = FilterString(carAdDetails[7]?.InnerText).Replace("Trim", "").Trim();
                    dubizzleCars.BodyCondititon = FilterString(carAdDetails[8]?.InnerText).Replace("Body Condition", "").Trim();
                    dubizzleCars.FuleType = FilterString(carAdDetails[10]?.InnerText).Replace("Fuel Type", "").Trim();
                    dubizzleCars.Color = FilterString(carAdDetails[11]?.InnerText).Replace("Color", "").Trim();
                    dubizzleCars.Doors = FilterString(carAdDetails[12]?.InnerText).Replace("Doors", "").Trim();
                    dubizzleCars.Cylenders = FilterString(carAdDetails[13]?.InnerText).Replace("Cylinders", "").Trim();
                    dubizzleCars.SallerType = FilterString(carAdDetails[14]?.InnerText).Replace("Seller Type", "").Trim();
                    dubizzleCars.Horsepower = FilterString(carAdDetails[15]?.InnerText).Replace("Horsepower", "").Trim();

                    dubizzleCars.Id = 0;
                    await _dubizzleCarsRepository.Save(dubizzleCars);
                }
                catch (Exception ex)
                {

                    throw;
                }
                #endregion

                //#region DubizzleFeaturedAdDetails....

                //var url = "https://dubai.dubizzle.com/motors/used-cars/?page=";
                //HtmlWeb web = new HtmlWeb();
                //var htmlDoc = await web.LoadFromWebAsync(url);
                //var carAdDetails = htmlDoc.DocumentNode.SelectNodes("//div[@class='promoted-ads__card-body']");
                //foreach (var item in carAdDetails)
                //{
                //    try
                //    {
                //        CarAds carAds = new CarAds();
                //        var innerNodeHtml = new HtmlAgilityPack.HtmlDocument();
                //        innerNodeHtml.LoadHtml(item.OuterHtml);
                //        var TitleNode = innerNodeHtml?.DocumentNode?.SelectSingleNode("//a") ?? null;
                //        carAds.LinkofCar = TitleNode?.Attributes["href"]?.Value ?? null;
                //        carAds.Title = FilterString(innerNodeHtml?.DocumentNode?.SelectSingleNode("//div[@class='promoted-ads__card-body__title']")?.InnerText);
                //        carAds.Id = 0;
                //        await _dubizzleCarsRepository.Save(dubizzleCars);
                //    }
                //    catch (Exception ex)
                //    {
                //        throw;
                //    }
                //}
                //#endregion

            }
        }
        public string FilterString(string text)
        {
            var responseData = text.Replace("\n", "").Replace("\t", " ").Replace("&#8234;", " ").Replace("&gt", " ").Replace("<strong>", " ").Replace("</strong>", " ").Replace("**", "").Replace("{", "").Replace("}", "").Trim().Replace("/", " ");
            return responseData;
        }
    }
}