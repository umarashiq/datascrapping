﻿using DataScrapping.DatabaseLayer.Model;
using DataScrapping.DatabaseLayer.Repositories;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DataScrappingApplication
{
    public partial class Website_Form : Form
    {

        private readonly IWebsiteRepository _websiteRepository;

        public Website_Form(IWebsiteRepository websiteRepository)
        {
            InitializeComponent();
            _websiteRepository = websiteRepository;

            Website_Form_Load();
        }

        private void save_button_Click(object sender, EventArgs e)
        {
            Website website = new Website();
            website.WebsiteName = WebsiteNameInput.Text;
            website.BaseUrl = WebsiteUrlInput.Text;

            if (website != null)
            {
                var data =_websiteRepository.Save(website);
                if (data >= 0)
                {
                    ResultShow.Text = "Data Inserted Successfully";
                }
                else
                {
                    ResultShow.Text = "Something Went Wrong...";
                }
            }
        }

        private void delete_button_Click(object sender, EventArgs e)
        {
            var Id = Convert.ToInt32(WebsiteDataGridView.SelectedRows[0]?.Cells[0]?.Value);
            var data = _websiteRepository.Delete(Id);
            if (data == true)
            {
                ResultShow.Text = "Record Deleted Successfully";
            }
            else
            {
                ResultShow.Text = "Something Went Wrong...";
            }
        }

        private void editBtn_Click(object sender, EventArgs e)
        {
            var Id = Convert.ToInt32(WebsiteDataGridView.SelectedRows[0]?.Cells[0]?.Value);
            var websiteName = Convert.ToString(WebsiteDataGridView.SelectedRows[0]?.Cells[1]?.Value);
            var websiteUrl = Convert.ToString(WebsiteDataGridView.SelectedRows[0]?.Cells[2]?.Value);

            Website website = new Website();
            website.Id = Id;
            website.WebsiteName = websiteName;
            website.BaseUrl = websiteUrl;

            if (website != null)
            {
                var data =_websiteRepository.Save(website);
                if (data >= 0)
                {
                    ResultShow.Text = "Data Updated Successfully";
                }
                else
                {
                    ResultShow.Text = "Something Went Wrong...";
                }
            }
        }

        private async void Website_Form_Load()
        {
            var data = await _websiteRepository.GetAll();
            foreach (var item in data)
            {
                WebsiteDataGridView.Rows.Add(item.Id, item.WebsiteName, item.BaseUrl);
            }
        }

        private void WebsiteDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}