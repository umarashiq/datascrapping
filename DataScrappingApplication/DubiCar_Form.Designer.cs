﻿
namespace DataScrappingApplication
{
    partial class DubiCar_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PageUrl = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.scrapDataBtn = new System.Windows.Forms.Button();
            this.Result = new System.Windows.Forms.Label();
            this.ShowResult = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // PageUrl
            // 
            this.PageUrl.Enabled = false;
            this.PageUrl.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.PageUrl.Location = new System.Drawing.Point(28, 134);
            this.PageUrl.Name = "PageUrl";
            this.PageUrl.Size = new System.Drawing.Size(785, 38);
            this.PageUrl.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 23);
            this.label2.TabIndex = 6;
            // 
            // scrapDataBtn
            // 
            this.scrapDataBtn.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.scrapDataBtn.Font = new System.Drawing.Font("Segoe UI Semibold", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.scrapDataBtn.Location = new System.Drawing.Point(311, 214);
            this.scrapDataBtn.Name = "scrapDataBtn";
            this.scrapDataBtn.Size = new System.Drawing.Size(182, 40);
            this.scrapDataBtn.TabIndex = 3;
            this.scrapDataBtn.Text = "Scrap Data";
            this.scrapDataBtn.UseVisualStyleBackColor = false;
            this.scrapDataBtn.Click += new System.EventHandler(this.button1_Click);
            // 
            // Result
            // 
            this.Result.AutoSize = true;
            this.Result.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.Result.Location = new System.Drawing.Point(25, 269);
            this.Result.Name = "Result";
            this.Result.Size = new System.Drawing.Size(61, 23);
            this.Result.TabIndex = 4;
            this.Result.Text = "Result:";
            // 
            // ShowResult
            // 
            this.ShowResult.AutoSize = true;
            this.ShowResult.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.ShowResult.Location = new System.Drawing.Point(120, 269);
            this.ShowResult.Name = "ShowResult";
            this.ShowResult.Size = new System.Drawing.Size(0, 23);
            this.ShowResult.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI Semibold", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(25, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(147, 23);
            this.label3.TabIndex = 8;
            this.label3.Text = "Enter Website Url:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI Semibold", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label4.Location = new System.Drawing.Point(262, 27);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(293, 32);
            this.label4.TabIndex = 9;
            this.label4.Text = "Dubi Cars Data Scrapping";
            // 
            // DubiCar_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(832, 345);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ShowResult);
            this.Controls.Add(this.Result);
            this.Controls.Add(this.scrapDataBtn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.PageUrl);
            this.Name = "DubiCar_Form";
            this.Text = "DubiCars";
            this.Load += new System.EventHandler(this.DubiCar_Form_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox PageUrl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button scrapDataBtn;
        private System.Windows.Forms.Label Result;
        private System.Windows.Forms.Label ShowResult;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}