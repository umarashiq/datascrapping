﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataScrapping.DatabaseLayer.Model
{
    public class CompanyModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int CompanyId { get; set; }
        public virtual Company Company { get; set; }
        public string ModelName { get; set; }
        public string AdsCount { get; set; }
        public string Url { get; set; }
        public virtual ICollection<SubModel> SubModel { get; set; }
        public DateTime UpdateDate { get; set; }
    }
}