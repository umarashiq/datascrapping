﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataScrapping.DatabaseLayer.Model
{
     public class Company
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string CompanyName { get; set; }
        public string AdsCount { get; set; }
        public string Url { get; set; }
        public virtual ICollection<CompanyModel> CompanyModel { get; set; }
        public DateTime UpdateDate { get; set; }
    }
}