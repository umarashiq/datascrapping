﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataScrapping.DatabaseLayer.Model
{
    public class SubModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int ModelId { get; set; }
        public virtual CompanyModel CompanyModel { get; set; }
        public string SubModelName { get; set; }
        public string AdsCount { get; set; }
        public string Url { get; set; }
        public DateTime UpdateDate { get; set; }
    }
}