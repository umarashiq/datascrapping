﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataScrapping.DatabaseLayer.Model
{
    public class Website
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string WebsiteName { get; set; }
        public string BaseUrl { get; set; }
        public virtual ICollection<CarAds> CarAds { get; set; }
    }
}