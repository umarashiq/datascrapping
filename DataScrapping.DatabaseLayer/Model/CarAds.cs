﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataScrapping.DatabaseLayer.Model
{
    public class CarAds
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [ForeignKey("Website")]
        public int WebsiteId { get; set; }
        public Website Website { get; set; }
        public string Title { get; set; }
        public string Make { get; set; }
        public string LinkofCar { get; set; }
        public string Model { get; set; }
        public string SellingPrice { get; set; }
        public string PostingDate { get; set; }
        public string MakeYear { get; set; }
        public string Millage { get; set; }
        public string Color { get; set; }
        public string Doors { get; set; }
        public string Warranty { get; set; }
        public string Specs { get; set; }
        public string Transmission { get; set; }
        public string BodyType { get; set; }
        public string Trim { get; set; }
        public string BodyCondititon { get; set; }
        public string FuleType { get; set; }
        public string Cylenders { get; set; }
        public string SallerType { get; set; }
        public string Horsepower { get; set; }
        public string Extras { get; set; }
        public string TechnicalFeatures { get; set; }
        public string ExportStatus { get; set; }
        public string Seats { get; set; }
        public string SteeringSide { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public DateTime UpdateDate { get; set; }
    }
}
