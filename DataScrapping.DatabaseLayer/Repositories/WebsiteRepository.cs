﻿
using DataScrapping.DatabaseLayer.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataScrapping.DatabaseLayer.Repositories
{
   public class WebsiteRepository : IWebsiteRepository
    {
        private readonly ScrapDataDBContext _scrappedDataDBContext;

        public WebsiteRepository(ScrapDataDBContext scrappedDataDBContext)
        {
            _scrappedDataDBContext = scrappedDataDBContext;
        }

        public async Task<List<Website>> GetAll()
        {
            return _scrappedDataDBContext.Website.ToList();
        }

        public long Save(Website model)
        {
            var saveData = _scrappedDataDBContext.Website.FirstOrDefault(x => x.BaseUrl.Equals(model.BaseUrl));
            if (saveData == null)
            {
                _scrappedDataDBContext.Website.Add(model);
            }
            else
            {
                var updateData = _scrappedDataDBContext.Website.FirstOrDefault(x => x.Id.Equals(model.Id));
                updateData.WebsiteName = model.WebsiteName;
                updateData.BaseUrl = model.BaseUrl;
            }
            _scrappedDataDBContext.SaveChangesAsync();
            return model.Id;
        }

        public bool Delete(int Id)
        {
            var result = _scrappedDataDBContext.Website.FirstOrDefault(x => x.Id.Equals(Id));
            if (result != null)
            {
                _scrappedDataDBContext.Website.Remove(result);
                _scrappedDataDBContext.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public interface IWebsiteRepository
    {
        Task<List<Website>> GetAll();
        long Save(Website model);
        bool Delete(int Id);
    }
}
