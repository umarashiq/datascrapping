﻿using DataScraping.Helpers;
using DataScrapping.DatabaseLayer.Helpers;
using DataScrapping.DatabaseLayer.Model;
using LinqKit;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataScrapping.DatabaseLayer.Repositories
{
   public class CarAdsRepository : ICarAdsRepository
    {
        private readonly ScrapDataDBContext _scrappedDataDBContext;
        public CarAdsRepository(ScrapDataDBContext scrappedDataDBContext)
        {
            _scrappedDataDBContext = scrappedDataDBContext;
        }
        public async Task<List<CarAds>> GetAll()
        {
            var data = await _scrappedDataDBContext.CarAds.Take(2000).OrderByDescending(x => x.Id).ToListAsync();
            return data;
        }
        public async Task<long> Save(CarAds model)
        {
            model.Id = 0;
            model.UpdateDate = DateTime.Now;
            var result = _scrappedDataDBContext.CarAds.FirstOrDefault(x => x.Title.Equals(model.Title) && x.SellingPrice.Equals(model.SellingPrice) && x.Millage.Equals(model.Millage));
            if (result == null)
                _scrappedDataDBContext.CarAds.Add(model);
            else
            {
                result.Make = model.Make;
                result.Title = model.Title;
                result.Model = model.Model;
                result.Color = model.Color;
                result.Specs = model.Specs;
                result.Seats = model.Seats;
                //result.Wheels = model.Wheels;
                result.Millage = model.Millage;
                //result.Interior = model.Interior;
                result.BodyType = model.BodyType;
                result.FuleType = model.FuleType;
                result.MakeYear = model.MakeYear;
                result.Location = model.Location;
                result.Cylenders = model.Cylenders;
                result.LinkofCar = model.LinkofCar;
                result.ExportStatus = model.ExportStatus;
                result.Transmission = model.Transmission;
                result.SteeringSide = model.SteeringSide;
                result.SellingPrice = model.SellingPrice;
                result.UpdateDate = model.UpdateDate;
            }
            await _scrappedDataDBContext.SaveChangesAsync();
            return model.Id;
        }

        public async Task<List<CarAds>> GetByFilters(SearchFilter filters)
        {
            List<CarAds> listData = new List<CarAds>();
            var predicate = PredicateBuilder.New<CarAds>(true);
            if (!string.IsNullOrEmpty(filters.SearchText))
            {
                predicate = predicate.And(i => i.Title.Contains(filters.SearchText));
                predicate = predicate.Or(i => i.Model.Contains(filters.SearchText));
                predicate = predicate.Or(i => i.Make.Contains(filters.SearchText));
                predicate = predicate.Or(i => i.Color.Contains(filters.SearchText));
                predicate = predicate.Or(i => i.Millage.Contains(filters.SearchText));
                predicate = predicate.Or(i => i.SellingPrice.Contains(filters.SearchText));
            }
            if (filters.Filters != null)
                foreach (var filter in filters.Filters)
                {
                    if (filter.Logic == "and")
                        predicate.And(DBHelper.BuildPredicate<CarAds>(filter.Name, filter.Operator, filter.Value));
                    else if (filter.Logic == "or")
                        predicate.Or(DBHelper.BuildPredicate<CarAds>(filter.Name, filter.Operator, filter.Value));
                    else
                        predicate.And(DBHelper.BuildPredicate<CarAds>(filter.Name, filter.Operator, filter.Value));
                }
            var direction = (filters.sortFilter?.Direction?.ToLower().Equals("asc") ?? false) ? DBHelper.Order.Asc : DBHelper.Order.Desc;
            var sortBy = (filters.sortFilter == null || String.IsNullOrEmpty(filters.sortFilter.SortBy)) ? "Id" : filters.sortFilter.SortBy;
            if (filters.Offset == 0 && filters.PageSize == 0)
                listData = await _scrappedDataDBContext.CarAds.Where(predicate).OrderByDynamic(sortBy, direction).ToListAsync();
            else
                listData = await _scrappedDataDBContext.CarAds.Where(predicate).OrderByDynamic(sortBy, direction).Skip(filters.Offset * filters.PageSize).Take(filters.PageSize).ToListAsync();
            return listData;
        }
    }
    public interface ICarAdsRepository
    {
        Task<List<CarAds>> GetAll();
        Task<List<CarAds>> GetByFilters(SearchFilter filterData);
        Task<long> Save(CarAds model);
    }
}
