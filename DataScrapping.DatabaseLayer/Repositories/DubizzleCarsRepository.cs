﻿using DataScrapping.DatabaseLayer.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataScrapping.DatabaseLayer.Repositories
{
    public class DubizzleCarsRepository : IDubizzleCarsRepository
    {
        private readonly ScrapDataDBContext _scrappedDataDBContext;
        public DubizzleCarsRepository(ScrapDataDBContext scrappedDataDBContext)
        {
            _scrappedDataDBContext = scrappedDataDBContext;
        }
        public async Task<List<CarAds>> GetAll()
        {
            return await _scrappedDataDBContext.CarAds.Where(x=> x.Id>2050).ToListAsync();
        }
        public async Task<long> Save(CarAds model)
        {
            try
            {
                model.UpdateDate = DateTime.Now;
                var result = _scrappedDataDBContext.CarAds.FirstOrDefault(x => x.Title.Equals(model.Title));
                if (result == null)
                    _scrappedDataDBContext.CarAds.Add(model);
                else
                {
                    result.Color = model.Color;
                    result.Doors = model.Doors;
                    result.LinkofCar = model.LinkofCar;
                    result.Location = model.Location;
                    result.MakeYear = model.MakeYear;
                    result.Millage = model.Millage;
                    result.PostingDate = model.PostingDate;
                    result.SellingPrice = model.SellingPrice;
                    result.Title = model.Title;
                    result.Make = model.Make;
                    result.Specs = model.Specs;
                    result.SteeringSide = "left";
                    result.TechnicalFeatures = model.TechnicalFeatures;
                    result.Transmission = model.Transmission;
                    result.Trim = model.Trim;
                    result.Warranty = model.Warranty;
                    result.SallerType = model.SallerType;
                    result.Horsepower = model.Horsepower;
                    result.ExportStatus = model.ExportStatus;
                    result.Extras = model.Extras;
                    result.FuleType = model.FuleType;
                    result.BodyCondititon = model.BodyCondititon;
                    result.BodyType = model.BodyType;
                    result.Seats = "N/A";
                    result.Cylenders = model.Cylenders;
                    result.WebsiteId = model.WebsiteId;
                    result.UpdateDate = model.UpdateDate;
                    //model.Id = result.Id;
                    //_scrappedDataDBContext.Entry<CarAds>(model).State = EntityState.Modified;
                    //_scrappedDataDBContext.CarAds.Update(model);
                }
                await _scrappedDataDBContext.SaveChangesAsync();
                return model.Id;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
    public interface IDubizzleCarsRepository
    {
        Task<List<CarAds>> GetAll();
        Task<long> Save(CarAds model);
    }
}
