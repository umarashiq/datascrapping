﻿using DataScrapping.DatabaseLayer.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataScrapping.DatabaseLayer.Repositories
{
   public class SubModelRepository: ISubModelRepository
    {
        private readonly ScrapDataDBContext _scrappedDataDBContext;
        public SubModelRepository(ScrapDataDBContext scrappedDataDBContext)
        {
            _scrappedDataDBContext = scrappedDataDBContext;
        }
        public async Task<List<SubModel>> GetAll()
        {
            return await _scrappedDataDBContext.SubModel.ToListAsync();
        }
        public async Task<long> Save(SubModel model)
        {
            model.UpdateDate = DateTime.Now;
            var result = _scrappedDataDBContext.SubModel.FirstOrDefault(x => x.SubModelName.Equals(model.SubModelName));
            if (result == null)
                _scrappedDataDBContext.SubModel.Add(model);
            else
            {
                model.Id = result.Id;
                _scrappedDataDBContext.Entry<SubModel>(model).State = EntityState.Modified;
                _scrappedDataDBContext.SubModel.Update(model);
            }
            await _scrappedDataDBContext.SaveChangesAsync();
            return model.Id;
        }
    }
    public interface ISubModelRepository
    {
        Task<List<SubModel>> GetAll();
        Task<long> Save(SubModel model);
    }
}
