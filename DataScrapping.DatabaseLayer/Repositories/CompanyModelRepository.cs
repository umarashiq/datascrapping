﻿using DataScrapping.DatabaseLayer.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace DataScrapping.DatabaseLayer.Repositories  
{
    public class CompanyModelRepository : ICompanyModelRepository
    {
        private readonly ScrapDataDBContext _scrappedDataDBContext;
        public CompanyModelRepository(ScrapDataDBContext scrappedDataDBContext)
        {
            _scrappedDataDBContext = scrappedDataDBContext;
        }
        public async Task<List<CompanyModel>> GetAll()
        {
            return await _scrappedDataDBContext.CompanyModel.OrderByDescending(x => x.Id).ToListAsync();
        }

        public async Task<long> Save(CompanyModel model)
        {
            try
            {
                model.UpdateDate = DateTime.Now;
                var result = _scrappedDataDBContext.CompanyModel.FirstOrDefault(x => x.ModelName.Equals(model.ModelName));
                if (result == null)
                    _scrappedDataDBContext.CompanyModel.Add(model);
                else
                {
                    result.ModelName = model.ModelName;
                    result.CompanyId = model.CompanyId;
                    result.AdsCount = model.AdsCount;
                    result.Url = model.Url;
                    result.UpdateDate = model.UpdateDate;
                }
                await _scrappedDataDBContext.SaveChangesAsync();
                return model.Id; 
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
    public interface ICompanyModelRepository
    {
        Task<List<CompanyModel>> GetAll();
        Task<long> Save(CompanyModel model);
    }
}
