﻿using DataScrapping.DatabaseLayer.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataScrapping.DatabaseLayer.Repositories
{
    public class CompanyRepository : ICompanyRepository
    {
        private readonly ScrapDataDBContext _scrappedDataDBContext;

        public CompanyRepository(ScrapDataDBContext scrappedDataDBContext)
        { _scrappedDataDBContext = scrappedDataDBContext; }

        public async Task<List<Company>> GetAll()
        {
            return await _scrappedDataDBContext.Company.Include(x => x.CompanyModel).ThenInclude(x => x.SubModel).ToListAsync();
        }
        public async Task<long> Save(Company model)
        {
            var result = _scrappedDataDBContext.Company.FirstOrDefault(x=> x.CompanyName.Equals(model.CompanyName));
            if (result==null)
                _scrappedDataDBContext.Company.Add(model);
            else
            {
                model.Id = result.Id;
                _scrappedDataDBContext.Entry<Company>(model).State = EntityState.Modified;
                _scrappedDataDBContext.Company.Update(model);
            }
            await _scrappedDataDBContext.SaveChangesAsync();
            return model.Id;
        }
    }
    public interface ICompanyRepository
    {
        Task<List<Company>> GetAll();
        Task<long> Save(Company model);
    }
}
