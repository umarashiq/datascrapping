﻿using DataScrapping.DatabaseLayer.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;

namespace DataScrapping.DatabaseLayer
{
    public class ScrapDataDBContext : DbContext
    {
        public ScrapDataDBContext(DbContextOptions<ScrapDataDBContext> options) : base(options)
        {
            ChangeTracker.LazyLoadingEnabled = false;
            ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("server=.;database=DataScrappingDB;trusted_connection=true;");
        }

        public virtual DbSet<Company> Company { get; set; }
        public virtual DbSet<CompanyModel> CompanyModel { get; set; }
        public virtual DbSet<SubModel> SubModel { get; set; }
        public virtual DbSet<Website> Website { get; set; }
        public virtual DbSet<CarAds> CarAds { get; set; }

    }
    public class BloggingContextFactory : IDesignTimeDbContextFactory<ScrapDataDBContext>
    {
        public ScrapDataDBContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<ScrapDataDBContext>();
            optionsBuilder.UseSqlServer("server=.;database=DataScrappingDB;trusted_connection=true;");

            return new ScrapDataDBContext(optionsBuilder.Options);
        }
    }
}
